#!/bin/bash --login

# Use the ruby 2.3.1
rvm use 2.3.1

# Use the demo gemset
rvm use 2.3.1@demo

# Install the gems package related to the Gemfile
bundle install

# Drop the database, restart to create a new database and do migration
rails db:drop db:create db:migrate

# Start the server up
rails s -b 0.0.0.0 -p 3000
